module.exports = {
  pathPrefix: `meetup/`,
  siteMetadata: {
    title: `Gatsby Default Starter`,
  },
  plugins: [`gatsby-plugin-react-helmet`],
}
